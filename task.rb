class String
  def valid?
    answer = []
    brackets = { '{' => '}', '[' => ']', '(' => ')', '<' => '>' }
    self.each_char do |char|
      answer << char if brackets.key?(char)
      return false if brackets.key(char) && brackets.key(char) != answer.pop
    end
  answer.empty?
  end
end
